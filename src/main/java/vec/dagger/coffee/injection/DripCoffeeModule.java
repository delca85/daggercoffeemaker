package vec.dagger.coffee.injection;

import javax.inject.Singleton;

import vec.dagger.coffee.CoffeeApp;
import vec.dagger.coffee.ElectricHeater;
import vec.dagger.coffee.Heater;
import dagger.Module;
import dagger.Provides;

@Module(
		injects = CoffeeApp.class,
		includes = PumpModule.class
	)

public class DripCoffeeModule {
	@Provides @Singleton Heater provideHeater(){
		return new ElectricHeater();
	}
}
