package vec.dagger.coffee.injection;

import vec.dagger.coffee.Pump;
import vec.dagger.coffee.Thermosiphon;
import dagger.Module;
import dagger.Provides;

@Module(complete = false, library = true)
public class PumpModule{
	@Provides Pump providePump(Thermosiphon pump){
		return pump;
	}
}
