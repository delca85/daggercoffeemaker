package vec.dagger.coffee;

public interface Heater {
	void on();
	void off();
	boolean isHot();

}
